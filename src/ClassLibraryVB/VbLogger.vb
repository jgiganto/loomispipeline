﻿Imports System.IO

Public NotInheritable Class VbLogger
    Private Shared ReadOnly Locker As New Object()

    Public Sub New()
    End Sub

    Public Sub LogIt(msg As String, ByVal logMessage As String, Optional path As String = "",
                     Optional ByVal isDebug As Boolean = False)
        If File.Exists(path) Then
            If IsDebug Then
                Debug.Print(DateTime.Now & "> | " & msg & " | " & logMessage)
            Else
                Using w As TextWriter = File.AppendText(path)
                    w.WriteLine(DateTime.Now & "> | " & msg & " | " & logMessage)
                    w.Flush()
                End Using
            End If
        Else
            If IsDebug Then
                Debug.Print(DateTime.Now & "> | " & msg & " | " & logMessage)
            Else
                SyncLock Locker
                    Using w As TextWriter = File.CreateText(path)
                        w.WriteLine(DateTime.Now & "> | " & msg & " | " & logMessage)
                        w.Flush()
                    End Using
                End SyncLock
            End If
        End If
    End Sub
End Class
