﻿using System;
using System.Windows.Forms;
using ClassLibraryVB;
using DebugTools.Tools;

namespace LoomisPoC
{
    public partial class LoomisExample : Form
    {
        private const string LogFileName = "VbLog.txt";
        private readonly VbLogger _logger = new VbLogger();
        private double _resultValue;
        private string _operationPerformed = "";
        private bool _isOperationPerformed;

        public LoomisExample()
        {
            InitializeComponent();
            Inspector inspector = new Inspector();
        }

        private void number_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            _logger.LogIt($"Button {button.Text}", $"Button {button.Text} pressed", AppDomain.CurrentDomain.BaseDirectory + LogFileName, false);
            
            if ((TextDisplay.Text == "0") || (_isOperationPerformed))
                TextDisplay.Clear();

            _isOperationPerformed = false;
            if (button.Text == ".")
            {
                if (!TextDisplay.Text.Contains("."))
                    TextDisplay.Text = TextDisplay.Text + button.Text;

            }
            else
                TextDisplay.Text = TextDisplay.Text + button.Text;
        }

        private void operator_click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            _logger.LogIt($"Button {button.Text}", $"Button {button.Text} pressed", AppDomain.CurrentDomain.BaseDirectory + LogFileName, false);

            if (_resultValue != 0)
            {
                buttonResult.PerformClick();
                _operationPerformed = button.Text;
                labelCurrentOperation.Text = _resultValue + " " + _operationPerformed;
                _isOperationPerformed = true;
            }
            else
            {
                _operationPerformed = button.Text;
                _resultValue = double.Parse(TextDisplay.Text);
                labelCurrentOperation.Text = _resultValue + " " + _operationPerformed;
                _isOperationPerformed = true;
            }
        }

        private void Reset_Number_Click(object sender, EventArgs e)
        {
            TextDisplay.Text = "0";
        }

        private void clear_Click(object sender, EventArgs e)
        {
            TextDisplay.Text = "0";
            _resultValue = 0;
        }

        private void result_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            _logger.LogIt($"Button {button.Text}", $"Button {button.Text} pressed", AppDomain.CurrentDomain.BaseDirectory + LogFileName, false);

            switch (_operationPerformed)
            {
                case "+":
                    TextDisplay.Text = (_resultValue + double.Parse(TextDisplay.Text)).ToString();
                    break;
                case "-":
                    TextDisplay.Text = (_resultValue - double.Parse(TextDisplay.Text)).ToString();
                    break;
                case "*":
                    TextDisplay.Text = (_resultValue * double.Parse(TextDisplay.Text)).ToString();
                    break;
                case "/":
                    TextDisplay.Text = (_resultValue / double.Parse(TextDisplay.Text)).ToString();
                    break;
                default:
                    break;
            }
            _resultValue = double.Parse(TextDisplay.Text);
            labelCurrentOperation.Text = "";
        }
    }
}
