﻿namespace LoomisPoC
{
    partial class LoomisExample
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.button7 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button0 = new System.Windows.Forms.Button();
			this.buttonPlus = new System.Windows.Forms.Button();
			this.buttonResult = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.TextDisplay = new System.Windows.Forms.TextBox();
			this.labelCurrentOperation = new System.Windows.Forms.Label();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AutoSize = true;
			this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanel1.ColumnCount = 4;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.Controls.Add(this.button7, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.button4, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.button1, 0, 4);
			this.tableLayoutPanel1.Controls.Add(this.button8, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.button5, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.button2, 1, 4);
			this.tableLayoutPanel1.Controls.Add(this.button9, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.button6, 2, 3);
			this.tableLayoutPanel1.Controls.Add(this.button3, 2, 4);
			this.tableLayoutPanel1.Controls.Add(this.button0, 1, 5);
			this.tableLayoutPanel1.Controls.Add(this.buttonPlus, 3, 4);
			this.tableLayoutPanel1.Controls.Add(this.buttonResult, 3, 5);
			this.tableLayoutPanel1.Controls.Add(this.buttonClear, 3, 2);
			this.tableLayoutPanel1.Controls.Add(this.TextDisplay, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.labelCurrentOperation, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 5;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 450);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// button7
			// 
			this.button7.AccessibleName = "button7";
			this.button7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button7.Location = new System.Drawing.Point(3, 83);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(194, 86);
			this.button7.TabIndex = 7;
			this.button7.Text = "7";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.number_Click);
			// 
			// button4
			// 
			this.button4.AccessibleName = "button4";
			this.button4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button4.Location = new System.Drawing.Point(3, 175);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(194, 86);
			this.button4.TabIndex = 4;
			this.button4.Text = "4";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.number_Click);
			// 
			// button1
			// 
			this.button1.AccessibleName = "button1";
			this.button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button1.Location = new System.Drawing.Point(3, 267);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(194, 86);
			this.button1.TabIndex = 1;
			this.button1.Text = "1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.number_Click);
			// 
			// button8
			// 
			this.button8.AccessibleName = "button8";
			this.button8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button8.Location = new System.Drawing.Point(203, 83);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(194, 86);
			this.button8.TabIndex = 8;
			this.button8.Text = "8";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.number_Click);
			// 
			// button5
			// 
			this.button5.AccessibleName = "button5";
			this.button5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button5.Location = new System.Drawing.Point(203, 175);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(194, 86);
			this.button5.TabIndex = 5;
			this.button5.Text = "5";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.number_Click);
			// 
			// button2
			// 
			this.button2.AccessibleName = "button2";
			this.button2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button2.Location = new System.Drawing.Point(203, 267);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(194, 86);
			this.button2.TabIndex = 2;
			this.button2.Text = "2";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.number_Click);
			// 
			// button9
			// 
			this.button9.AccessibleName = "button9";
			this.button9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button9.Location = new System.Drawing.Point(403, 83);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(194, 86);
			this.button9.TabIndex = 9;
			this.button9.Text = "9";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.number_Click);
			// 
			// button6
			// 
			this.button6.AccessibleName = "button6";
			this.button6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button6.Location = new System.Drawing.Point(403, 175);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(194, 86);
			this.button6.TabIndex = 6;
			this.button6.Text = "6";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.number_Click);
			// 
			// button3
			// 
			this.button3.AccessibleName = "button3";
			this.button3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button3.Location = new System.Drawing.Point(403, 267);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(194, 86);
			this.button3.TabIndex = 3;
			this.button3.Text = "3";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.number_Click);
			// 
			// button0
			// 
			this.button0.AccessibleName = "button0";
			this.button0.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.button0.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button0.Location = new System.Drawing.Point(203, 359);
			this.button0.Name = "button0";
			this.button0.Size = new System.Drawing.Size(194, 88);
			this.button0.TabIndex = 10;
			this.button0.Text = "0";
			this.button0.UseVisualStyleBackColor = true;
			this.button0.Click += new System.EventHandler(this.number_Click);
			// 
			// buttonPlus
			// 
			this.buttonPlus.AccessibleName = "buttonPlus";
			this.buttonPlus.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.buttonPlus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.buttonPlus.Location = new System.Drawing.Point(603, 267);
			this.buttonPlus.Name = "buttonPlus";
			this.buttonPlus.Size = new System.Drawing.Size(194, 86);
			this.buttonPlus.TabIndex = 11;
			this.buttonPlus.Text = "+";
			this.buttonPlus.UseVisualStyleBackColor = true;
			this.buttonPlus.Click += new System.EventHandler(this.operator_click);
			// 
			// buttonResult
			// 
			this.buttonResult.AccessibleName = "buttonResult";
			this.buttonResult.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.buttonResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.buttonResult.Location = new System.Drawing.Point(603, 359);
			this.buttonResult.Name = "buttonResult";
			this.buttonResult.Size = new System.Drawing.Size(194, 88);
			this.buttonResult.TabIndex = 12;
			this.buttonResult.Text = "=";
			this.buttonResult.UseVisualStyleBackColor = true;
			this.buttonResult.Click += new System.EventHandler(this.result_Click);
			// 
			// buttonClear
			// 
			this.buttonClear.AccessibleName = "buttonClear";
			this.buttonClear.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.buttonClear.Dock = System.Windows.Forms.DockStyle.Fill;
			this.buttonClear.Location = new System.Drawing.Point(603, 83);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(194, 86);
			this.buttonClear.TabIndex = 13;
			this.buttonClear.Text = "C";
			this.buttonClear.UseVisualStyleBackColor = true;
			this.buttonClear.Click += new System.EventHandler(this.clear_Click);
			// 
			// TextDisplay
			// 
			this.TextDisplay.AccessibleName = "TextDisplay";
			this.TextDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.TextDisplay.BackColor = System.Drawing.Color.White;
			this.tableLayoutPanel1.SetColumnSpan(this.TextDisplay, 4);
			this.TextDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TextDisplay.Location = new System.Drawing.Point(3, 28);
			this.TextDisplay.Name = "TextDisplay";
			this.TextDisplay.ReadOnly = true;
			this.TextDisplay.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.TextDisplay.Size = new System.Drawing.Size(793, 49);
			this.TextDisplay.TabIndex = 14;
			// 
			// labelCurrentOperation
			// 
			this.labelCurrentOperation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.labelCurrentOperation.AutoSize = true;
			this.tableLayoutPanel1.SetColumnSpan(this.labelCurrentOperation, 4);
			this.labelCurrentOperation.Location = new System.Drawing.Point(777, 6);
			this.labelCurrentOperation.Margin = new System.Windows.Forms.Padding(10, 6, 10, 6);
			this.labelCurrentOperation.Name = "labelCurrentOperation";
			this.labelCurrentOperation.Size = new System.Drawing.Size(13, 13);
			this.labelCurrentOperation.TabIndex = 15;
			this.labelCurrentOperation.Text = "0";
			// 
			// LoomisExample
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "LoomisExample";
			this.Text = "LoomisExample";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button buttonResult;
        private System.Windows.Forms.Button buttonPlus;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.TextBox TextDisplay;
		private System.Windows.Forms.Label labelCurrentOperation;
	}
}

