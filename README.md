# Loomis Pipelines

![](./doc/images/loomisLogo.png)

## Index

- [Loomis Pipelines](#loomis-pipelines)
  - [Index](#index)
  - [Status](#status)
  - [1. Get started](#1-get-started)
    - [VPN](#vpn)
    - [Set up runner with windows server](#set-up-runner-with-windows-server)
  - [2. Branching strategy](#2-branching-strategy)
  - [3. GitLab Pipeline](#3-gitlab-pipeline)
  - [4. Automatic Building](#4-automatic-building)
  - [5. Testing](#5-testing)
  - [6. Code analysis](#6-code-analysis)
  - [7. Automatic build installer](#7-automatic-build-installer)
  - [8. Deploying](#8-deploying)
  - [9. Data Base version control](#9-data-base-version-control)
  - [10. Links](#10-links)

## Status

[x] Creation of the first pipeline which automatic compilations copying the binaries files in a shared folder.  
[x] WinAppDriver automatic tests.  
[x] GitLab configuration for integrating in the flow.  
[x] Transfer of the project in GitLab to the Loomis team.  
[x] Example of WinAppDriver test case integrated into the flow.  
[x] Add SonarQube

## 1. Get started

### VPN

Loomis use forticlient to connect with their systems.

1. Download and install [FortiClient](https://forticlient.com) in your computer from and configure it with the next settings:
   - Connection name: **Loomis AB**
   - Description: **Loomis AB**
   - Remote gateway: **https://forti.loomis.com:433**
   - Client certificate: **None**
   - Username: _The user name provided from Loomis_
1. You have to provide a _phone number_ to the Loomis IT department and they will send you the password for your user by SMS.
1. After get the password you will need one more thing, the token, it will provided for the Fortitoken application but you need first a QR code from the IT department.
1. Install the application FortiToken in your smart device from Google play or AppStore (Two-factor authentication)
1. Scan the **QR-code** that you will receive in a separate e-mail into the FortiToken app to activate it. This needs to be done within 24 hours from receiving it.
1. Get the token set it in the forticlient application. And login.
1. Done!

[⬆️ Go to index](#index)

### Set up runner with windows server

- [Install GitLab Runner on Windows](https://docs.gitlab.com/runner/install/windows.html). Ideally, the GitLab Runner should not be installed on the same machine as GitLab.
  Read the [requirements documentation](https://gitlab.com/help/install/requirements.md#gitlab-runner) for more information. - Configure the runner as shell. - Add therunner as service
- Install **vs_BuildTools.exe** placed in the folder _tools_ of the repository.
- Enable [Developer mode](https://docs.microsoft.com/es-es/windows/uwp/get-started/enable-your-device-for-development)
- Copy the **Nuget** executable (placed in the folder _tools_ in the repository) into _"C:\Program Files (x86)\NuGet"_.
- Install the **NUnit** console from the floder _tools_ originally downloaded from [here](https://github.com/nunit/nunit-console/releases).
- Enabling the feature for [watch pipelines reports](https://docs.gitlab.com/ee/ci/junit_test_reports.html#how-it-works). This feature comes with the :junit_pipeline_view feature flag disabled by default. This feature is disabled due to some performance issues with very large data sets. When the performance issue is resolved, the feature will be enabled by default. To enable this feature, ask a GitLab administrator with Rails console access to run the following command:

```shell
Feature.enable(:junit_pipeline_view)
```

- Install **[Advanced Installer](https://www.advancedinstaller.com/download.html)** and set up the licence for the current machine.
- Install **[SonarQube analyzer](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-msbuild/)** according with the link and decompress it in the path _C:\Program Files (x86)\sonar-scanner-msbuild_
  - Token: **602707c09b1c91afcc16051226f7a5df90914767**

```xml
  <Property Name="sonar.host.url">http://localhost:9000</Property>
  <Property Name="sonar.login">admin</Property>
  <Property Name="sonar.password">admin</Property>
```

- Install **[Windows application driver](https://github.com/microsoft/WinAppDriver/releases)**
- Add the following paths to the environment variables in windows, and checking ig all this software exist in their

  - C:\Program Files (x86)\NuGet;
  - C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\MSBuild\Current\Bin;
  - C:\Program Files (x86)\NUnit.org\nunit-console\
  - C:\Program Files (x86)\Caphyon\Advanced Installer 16.9\bin\x86
  - C:\Program Files (x86)\sonar-scanner-msbuild
  - C:\Program Files (x86)\Windows Application Driver

- Test the msbuild and the nuget applications openning a terminal:

```powershell
PS C:\> msbuild
Microsoft (R) Build Engine version 16.5.0+d4cbfca49 for .NET Framework
Copyright (C) Microsoft Corporation. All rights reserved.

MSBUILD : error MSB1003: Specify a project or solution file. The current working directory does not contain a project or solution file.
```

- Ensure the **nuget.exe** is in the right path in the runner machine, and the **msbuild** path, this is the way the runner can have access to the needed resouces.

```powershell
PS C:\> nuget
NuGet Version: 4.4.3.5892
usage: NuGet <command> [args] [options]
Type 'NuGet help <command>' for help on a specific command.
```

- Ensure aslo you placed the test console used **NUnit console** in the case, this executable should be reached for the pipeline in the **NUNIT_PATH** path.

```powershell
PS C:\> nunit3-console
NUnit Console Runner 3.11.1 (.NET 2.0)
Copyright (c) 2020 Charlie Poole, Rob Prouse
jueves, 26 de marzo de 2020 23:26:48
```

- Ensure Advanced installer path it also working.

```powershell
PS C:\> AdvancedInstaller.com /?
Advanced Installer 16.9

Usage:
  AdvancedInstaller.com <path_to_project_file>
```

- Ensure SonarQube analizer path it also working.

```powershell
PS C:\> SonarScanner.MSBuild
SonarScanner for MSBuild 4.7.1
Using the .NET Framework version of the Scanner for MSBuild
```

- Ensure WinAppDriver path it also working.

```powershell
PS C:\> WinAppDriver
Windows Application Driver listening for requests at: http://127.0.0.1:4723/
Press ENTER to exit.
```

- The pipeline will be built automatically from the file _.gitlab-ci.yml_ hosted in the repository.

[⬆️ Go to index](#index)

## 2. Branching strategy

Gitlab will be the main project management tool. This server used on premise.  
GitLab is a web-based DevOps life cycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration/continuous deployment pipeline features, using an open-source license.  
It follows an open-core development model where the core functionality is released under an open-source (MIT) license while the additional functionality is under a proprietary license.
The Gitlab project are mono-repository this made this push the development to no divide the project into several repositories.

The branching strategy was defined in the document **"Loomis Branch strategy.docx"**.

[⬆️ Go to index](#index)

## 3. GitLab Pipeline

The automation is based in a only one pipeline structure which works in two different ways.

- `Continous Integration (CI)`: Every `push` in the _develop_ branch, _master_ branch or any merge of `Merge Request` trigger which automatic builds and check every new code in the repository. This will compose of **Build + tests + code analysis**. _Sonarqube_ will be always updated with the last status of the project and we will have a sanity check of our project in every moment.

```cmd
git push origin develop
```

```cmd
git push origin master
```

<p align="center">
  <img alt="GitLabPushFlow" src="./doc/images/GitLabPushFlow.png" width="650px">
</p>

- `New version publishing (CD)`: In every push of a tagged version of our code to master, it will be triggered the pipeline, but in this case, we will activate some additional task.  
  In this case, the pipeline will run **Build + Tests + Code analysis + Installer building + Deploying**. This will generate all the necessary files to delivering a new version of the system in the different environments.

```cmd
git tag -m "Version minor" -a "1.1.0"
git push origin 1.1.0
```

![ ](./doc/images/GitLabTagFlow.png)

## 4. Automatic Building

The GitLab Runner was configured with MSBuild 2019, this tool can compile **Visual Basic .NET**, **C#** and different versions of the framework .NET: The targect is to compile _Securcash_, _SecurcashCOE_ and tools which use .NET Framework 3.5 y 4.0 and 4.5.2.
Te compile task is divided in 2 stages:

- Restore the Nuget packages: with the command `restore` which the will restore all the Nuget dependencies based in the file **Nuget.config**.

```powershell
Nuget restore
```

- Compile using MSBuild and the result will be placed in the different folders, configured in the **\*.csproj** and **\*.vbproj** of each project. We will place in `bin` all the binaries and depending of the configuration `bin\release`, we also separate the **tests**, **desktop application** and **WCF service** y different folders. Like `bin\release\wcf` or `bin\release\app`.

```powershell
MsBuild /p:Configuration=Release /clp:Summary /p:DebugSymbols=false /p:DebugType=None
```

We will include as parallel build tasks as solutions in the system (for example 1 per the _.NET Forms_ application and another per _ASP\.NET WCF Service_)

Every project will be configured as output in the **bin/Release/Front** in the case the project belong to Front, and **bin/Release/Back** in the case of the project WCF service. This allow us to separate in different folders the code of different artifacts.

```xml
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' ">
...
    <OutputPath>$(SolutionDir)bin\Release\Front\</OutputPath>
```

[⬆️ Go to index](#index)

## 5. Testing

The front will pass 3 different kind of tests:

- **Unit tests:** In the example are several [unit tests](https://docs.microsoft.com/es-es/dotnet/core/testing/unit-testing-with-dotnet-test) in both languages, c# and vb
- **Integration tests:** Not in the target of this project but the execution and the scripting in the pipeline are exactly the same than unit tests.
- **Test end to end (e2e):** Running in a different job of the pipeline. Needs an specific runtime, implemented with [Windows Application Driver](https://github.com/microsoft/WinAppDriver)

Additionally the job of the analyzer of **SonnarQube** will be triggered in the _test stage_.

the execution of the test will be made by [multi assembly](https://nunit.org/docs/2.4/consoleCommandLine.html), executed by NUnit.

```powershell
nunit-console assembly1.dll assembly2.dll assembly3.dll
```

The powershell script will read the folder _bin/test_ and will find the assemblies with the _test.dll_ postfix

```powershell
$env:NUNIT_PATH `"((Get-ChildItem $env:TEST_FOLDER\*test.dll) -split "`n" -join "`" `"")`"'
```

[⬆️ Go to index](#index)

## 6. Code analysis

For code analysis we will use [SonarQube](https://www.google.com/search?client=firefox-b-d&q=sonarqube). SonarQube (formerly Sonar) is an open-source platform developed by SonarSource for continuous inspection of code quality to perform automatic reviews with static analysis of code to detect bugs, code smells, and security vulnerabilities. SonarQube offers reports on duplicated code, coding standards, unit tests, code coverage, code complexity, comments, bugs, and security vulnerabilities.

The Sonar analyzer is installed in the machine with the runner and executed though a Job, when this job finish it send a report to the [Sonar Server](http://sonarcube.global.cashmgmt.net/), this colLects all the information and publish the results of the analysis.

- Url sonar server: **http://sonarcube.global.cashmgmt.net/**

The Sonar server **automatically apply the default rules** in our project according the language analyzed. Default rules are suitable for
the project in the mostly of the cases.  
Sonar also can integrate in Visual Studio for analyzing the code the IDE, the plugin is called [sonarlint](https://www.sonarlint.org/visualstudio/).

[⬆️ Go to index](#index)

## 7. Automatic build installer

The automation is done through **Advanced Installer**, this will gives us several advantages.

- Very intuitive and usable interface
- Ability to automatization thanks to the command line
- To be able to change the [permissions of the folders by the installer](https://www.advancedinstaller.com/user-guide/permissions.html)
- Version of .msi
- Conversion Wise and Wix projects

The idea is to have an Advanced Installer project common between all future versions and to modify that project every time there is a new version. Will will do this with the command line of Advanced installer and a job of Gitlab.

The first step is to configure a generic project (.aip) that will work to build the installer for every environment, although another strategy is to configure several installer projects. This file with _aip_ extension will be saved in the `/installer` folder of our repository.  
Once configured, there will be parameters that we will need to change every time we generate a new version, such as the version number, the language, or the files that will be automatically included when configuring the root folder.

Each GitLab Job will be configured to add the corresponding parameters to its version and environment.

- [Modify version](https://www.advancedinstaller.com/user-guide/command-line.html#example)
- [Add configuration files](https://www.advancedinstaller.com/user-guide/add-file.html) corresponding to the environment

```powershell
- '& $env:ADVINSTALLER_PATH /edit $env:MSI_PROJECT_FILE /SetVersion $env:CI_BUILD_TAG'
## This line indicates how we can to add a different configuration file to the installer
- '& $env:ADVINSTALLER_PATH /edit $env:MSI_PROJECT_FILE /AddFile "<Config file path according nvironment>""'
- '& $env:ADVINSTALLER_PATH /edit $env:MSI_PROJECT_FILE /SetOutputLocation -buildname DefaultBuild -path $env:MSI_RELEASE_FOLDER'
- '& $env:ADVINSTALLER_PATH /build $env:MSI_PROJECT_FILE'
```

We will have so many Jobs as environment configurations.

[⬆️ Go to index](#index)

## 8. Deploying

As we don't have access to the different environments, the pipeline doesn't deploy directly to the environments, the results of the different publications will be copied to a shared folder or to any other convenient folder.
In this folder, which we'll generate with the name of the version, we'll copy the installers generated in the previous steps here and the folder of the release of the WebService.
This line get the name for the folder with the version number:

```powershell
'$deployFolder = $($env:DEPLOY_FOLDER) + "\" + $($env:CI_BUILD_TAG) + " - " + $commitSubject + "\"'
```

The next step is to generate a **publish** of the service. The right result of building a **publish** the _WCF service_ will be the base of the new version, we must have the corresponding configuration files in a folder and when the version is generated, it should be copied, so many times as different environments we have and add the corresponding configuration file to every environment.  
For example, the configuration files that contain the database connection string and other configuration and data. So it should include the configuration files in each installation package after build the publishing.  
The next line corresponding to a post on MSBuild.

```powershell
msbuild  /p:DeployOnBuild=true /p:PublishProfile=CustomProfile /p:outdir="outputFolder\\" src\WcfService1\WcfService1.csproj
```

After building the **publish**, we must copy the result to the corresponding folders and replace the default configuration files with the corresponding ones for every configuration environment.  
This would be done with a simple copy command.

```powershell
'xcopy /y "$env:RELEASE_FOLDER" $(´""$deployFolder`"")'
'xcopy /y "`".\$env:MSI_RELEASE_FOLDER\`"" "`"$env:DEPLOY_FOLDER + "\" + $deployFolder`""'
'xcopy /y ".\TestResult.xml" "$deployFolder"'
```

The result will depend on what we want to obtain:

- If we want a folder with all the installers and another with all the **publish builds**.
- Or on the contrary, a folder for **each environment** that would contain the _frontend installer_ and the publishing of the properly configured service.

[⬆️ Go to index](#index)

## 9. Data Base version control

Usually the industry use [CRM's](https://en.wikipedia.org/wiki/Object-relational_mapping) which it can transform code to relational objects of a database, with this power, the CRM can control the database, populate it, and generate scripts which can be executed to modify the database or return to previous versions.

For .NET the most used is [Entity Framework](https://docs.microsoft.com/en-gb/dotnet/framework/data/adonet/ef/overview) which control the database status with [migrations](https://www.learnentityframeworkcore.com/migrations).  
This requires major changes in a project that today is legacy code, so Discarding a solution which requires to refactor a big part of the legacy code, it's recommended to find another solution to manage database changes in the repository.

One immediate solution is to use a tool to help with the process of merge the changes which every developer does by centralizing the changes in the repository for the developers which they work in their local databases can recover the changes of other developers, getting the changes from the repository. This will maintain the databases synchronized and the process of making a script for a new version will be more simple, only recovering the scripts from the repository.

<p align="center">
  <img alt="Rust" src="doc/images/DataBaseSynchro.png" width="450px">
</p>

Two possible software to perform this function is [Red Gate Sql Source Control](https://www.red-gate.com/products/sql-development/sql-source-control/) or [Devart Source Control](https://docs.devart.com/source-control/index.html).

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/Uz0sH3jL2TM/0.jpg)](https://youtu.be/Uz0sH3jL2TM)

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/nTEGHeYVmik/0.jpg)](https://www.youtube.com/watch?v=nTEGHeYVmik)

Once tried the tool, the repository created and managed by this tool would have [this aspect](https://github.com/kenny-reyes/DatabaseGit).  
There are also [other similar tools](https://dbmstools.com/categories/version-control-tools/sqlserver) where we can find a similar solution.

These products are sold on the subscription basis, i.e. during a year after a purchase, you can download new versions released within this period. But the license itself is permanent in the sense, that a particular version can be used without any time limitations.

[⬆️ Go to index](#index)

## 10. Links

Some links which could be useful:

- [SonarQube short tutorial](https://blog.setapp.pl/sonarqube_introduction)
- [Advanced installer command line help](https://www.advancedinstaller.com/user-guide/command-line.html)
- [Advanced Installer Command line for editing projects](https://www.advancedinstaller.com/user-guide/command-line-editing.html)
- [Video example of Advanced Installer automatization](https://www.advancedinstaller.com/powershell-automation.html)
- [Introduction to Windows Application Driver an Selenium like tools for Window app automation](https://www.youtube.com/watch?v=p6AEqaRi0PM)
- [Identify and work with Windows UI element using WinAppDriver](https://www.youtube.com/watch?v=EW6lYxVO2ow)
- [Working with Nested Windows UI element identification in Windows App Driver](https://www.youtube.com/watch?v=0yaoDB2Y9w4)
- [Introducing GitFlow](https://datasift.github.io/gitflow/IntroducingGitFlow.html)
- [Sonar Scanner for MSBuild](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-msbuild/)

[⬆️ Go to index](#index)
