﻿using NUnit.Framework;

namespace WinAppDriver.Test
{
    // https://xunit.net/docs/comparisons
    [TestFixture]
    public class ScenarioStandard
    {
        private const string ButtonResult = "buttonResult";
        private const string TextDisplay = "TextDisplay";

        private static ApplicationSession _appSession;
        
        [Test(Description = "Trying the buttons of the calculator with the Add operation")]
        [TestCase("1", "buttonPlus", "2")]
        [TestCase("3", "buttonPlus", "4")]
        [TestCase("5", "buttonPlus", "6")]
        [TestCase("7", "buttonPlus", "8")]
        [TestCase("9", "buttonPlus", "0")]
        public void OperatorAdd(string input1, string operation, string input2)
        {
            // Run sequence of button presses specified above and validate the results
            _appSession.Session.FindElementByName($"button{input1}").Click();
            _appSession.Session.FindElementByName(operation).Click();
            _appSession.Session.FindElementByName($"button{input2}").Click();
            _appSession.Session.FindElementByName(ButtonResult).Click(); 

            var givenResult = _appSession.Session.FindElementByName(TextDisplay).Text;

            Assert.AreEqual(givenResult, (int.Parse(input1) + int.Parse(input2)).ToString());
        }

        [SetUp]
        public void TestInitialize()
        {
            _appSession.Cleanup();
        }

        [OneTimeSetUp]
        public static void ClassInitialize()
        {
            _appSession = new ApplicationSession();
        }

        [OneTimeTearDown]
        public static void ClassCleanup()
        {
            _appSession.Dispose();
        }
    }
}

