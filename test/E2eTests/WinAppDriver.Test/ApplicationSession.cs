﻿using System;
using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;

namespace WinAppDriver.Test
{
    public class ApplicationSession :IDisposable
    {
        private const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";
        private const string AppToTest = "LoomisPoC.exe";
        private const string AppName = "LoomisExample";

        public WindowsDriver<WindowsElement> Session { get; }

        public WindowsElement ApplicationResultDisplay { get; private set; }

        public ApplicationSession()
        {
            string testFolder = AppDomain.CurrentDomain.BaseDirectory + "\\";

            // Create a new session to launch Application application
            DesiredCapabilities appCapabilities = new DesiredCapabilities();
            appCapabilities.SetCapability("app", testFolder + AppToTest);
            appCapabilities.SetCapability("appWorkingDir", testFolder);
            this.Session = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appCapabilities);
            Assert.IsNotNull(Session);
            Assert.IsNotNull(Session.SessionId);

            // Verify that Application is started with untitled new file
            Assert.AreEqual(AppName, Session.Title);

            // Set implicit timeout to 1.5 seconds to make element search to retry every 500 ms for at most three times
            Session.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1.5);

            ApplicationResultDisplay = Session.FindElementByName("TextDisplay");
        }

        public void Cleanup()
        {
            Session.FindElementByName("buttonClear").Click();

            Assert.AreEqual("0", ApplicationResultDisplay.Text);
        }
        
        public void Dispose()
        {
            // Close the application and delete the session
            if (Session != null)
            {
                ApplicationResultDisplay = null;

                Session.Close();

                Session.Quit();
            }
        }
    }
}